
from typing import TypeAlias
from functools import lru_cache
from pkg_resources import resource_filename  # type: ignore
import pickle as pkl

import numpy as np

Antichain: TypeAlias = tuple[tuple[int, ...], ...]

@lru_cache
def load_achains(n) -> list[Antichain]:
    """Load the precomputed antichains for n source variables.
    
    lattice.pkl currently contains antichains for n <= 5.
    """

    lattice_file_name = resource_filename(__name__, "lattice.pkl")

    with open(lattice_file_name, "rb") as lattice_file:
        lattices = pkl.load(lattice_file)

    try:
        achains = lattices[n][0]
    except KeyError:
        raise ValueError(f"No antichains found for {n=}.")
    
    return [tuple(tuple(i-1 for i in a) for a in alpha) for alpha in achains]


@lru_cache
def load_moebius_inversion_matrix(n) -> np.ndarray:
    """Load the precomputed inversion matrix for n source variables.
        The order of the rows and columns of the matrix is the same as the order of the antichains in load_achains.
        
        lattice.pkl currently contains inversion matrices for n <= 5.
    """

    moebius_file_name = resource_filename(__name__, "lattice.pkl")

    with open(moebius_file_name, "rb") as moebius_file:
        moebius_data = pkl.load(moebius_file)

    try:
        matrix = moebius_data[n][1]
    except KeyError:
        raise ValueError(f"No inversion matrix found for {n=}.")

    return np.array(matrix)