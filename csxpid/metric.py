from enum import Enum
from typing import cast, TypeAlias, Callable

import numpy as np

Norm: TypeAlias = Callable[[np.ndarray], np.ndarray]
"""Norms act on the last axis of the array."""

Metric: TypeAlias = Callable[[np.ndarray, np.ndarray], np.ndarray]
"""Metrics act on the last axis of the array."""

class Norms(Enum):
    EUCLIDIAN: Norm = lambda x:  np.linalg.norm(x, axis=-1)
    MAX: Norm = lambda x: np.max(np.abs(x), axis=-1)

def composite_norm(outer: Norm, inner: list[Norm], dimensions: list[int]) -> Norm:
    """Create a composite norm that computes the outer_norm of the inner_norms."""
    cum_dims = np.cumsum(dimensions)[:-1]
    def norm(x: np.ndarray) -> np.ndarray:
        
        inner_lengths = []
        for x_i, d_i in zip(np.split(x, cum_dims, axis=-1), inner, strict=True):
            inner_lengths.append(d_i(x_i))
        inner_lengths = np.stack(inner_lengths, axis=-1)
        return outer(inner_lengths)
    return norm

def to_metric(norm: Norm) -> Metric:
    """Create a metric that computes the norm of the array."""
    return lambda x, y: norm(x - y)