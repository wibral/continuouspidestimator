from typing import Protocol

import numpy as np
from scipy.spatial import KDTree

from csxpid.metric import Metric, Norms, composite_norm, to_metric
from csxpid.random_variable import RandomVariable


class KNNFinder(Protocol):
    """A class that finds the k nearest neighbors of a query vector in the given data."""

    def __init__(self, rvs: list[RandomVariable], data: np.ndarray) -> None:
        """Initialise the KNN_Finder with the given data.

        Args:
            rvs: List of RandomVariables specifying the dimensionalities of the data.
            data: The data to find the nearest neighbors of the query vector. Data is a numpy array of shape (n_samples, sum_rv_dimensions).
        """
        raise NotImplementedError

    def get_neighbors(self, query: np.ndarray, k: int) -> np.ndarray:
        """Find the k nearest neighbors of the given query vector.
        May include the query vector itself if it is in the data.

        Returns:
            Tuple (distances, indices) of the k nearest neighbors of the query vectors.
        """
        raise NotImplementedError

    def get_neighbors_ball(self, query: np.ndarray, eps: np.ndarray) -> np.ndarray:
        """Find the neighbors of the given query vector that are strictly (<) within the given epsilon.
        May include the query vector itself if it is in the data.

        Returns:
            Indices of the neighbors as np array.
        """
        raise NotImplementedError

    def count_neighbors(self, query: np.ndarray, eps: np.ndarray) -> np.ndarray:
        """Count the number of neighbors of the given query vector that are within the given epsilon.

        Returns:
            Number of neighbors.
        """
        raise NotImplementedError


class SciPyKDTreesKNNFinder():
    """A class that finds the k nearest neighbors of a query vector in the given data using scipy.spatial.KDTree."""

    def __init__(self, rvs: list[RandomVariable], data: np.ndarray, workers: int = 1) -> None:

        assert all(rv.measurable_space.dimension == 1 or rv.measurable_space.norm == Norms.MAX for rv in rvs), \
            "All random variables must use the maximum norm for SciPy kd-trees."

        self._workers = workers
        self._kdt = KDTree(data)

    def get_neighbors(self, query: np.ndarray, k: int) -> np.ndarray:
        return self._kdt.query(query, k=k, p=np.inf, workers=self._workers)

    def get_neighbors_ball(self, query: np.ndarray, eps: np.ndarray) -> np.ndarray:
        return self._kdt.query_ball_point(query, np.nextafter(eps, 0), p=np.inf, workers=self._workers)

    def count_neighbors(self, query: np.ndarray, eps: np.ndarray) -> int:
        return self._kdt.query_ball_point(query, r=np.nextafter(eps, 0), p=np.inf, return_length=True, workers=self._workers)


class BruteForceKNNFinder():
    """A class that finds the k nearest neighbors of a query vector in the given data using a brute force search."""

    def __init__(self, rvs: list[RandomVariable], data: np.ndarray) -> None:

        self._data: np.ndarray = data
        self._metric: Metric = to_metric(composite_norm(outer=Norms.MAX, inner=[
                                         rv.measurable_space.norm for rv in rvs], dimensions=[rv.measurable_space.dimension for rv in rvs]))

    def get_neighbors(self, query: np.ndarray, k: int) -> np.ndarray:
        distances = self._metric(query[:, np.newaxis], self._data[np.newaxis])

        kNN_idx = np.argpartition(distances, k, axis=1)[:, :k]
        kNN_dists = np.take_along_axis(distances, kNN_idx, axis=1)

        knn_idx_idx = np.argsort(kNN_dists, axis=1)

        indices = np.take_along_axis(kNN_idx, knn_idx_idx, axis=1)
        distances = np.take_along_axis(kNN_dists, knn_idx_idx, axis=1)

        return distances, indices

    def get_neighbors_ball(self, query: np.ndarray, eps: np.ndarray) -> np.ndarray:
        distances = self._metric(query[:, np.newaxis], self._data[np.newaxis])

        neighbors = [np.nonzero(distances_point < r)[0]
                     for r, distances_point in zip(eps, distances)]
        return np.array(neighbors, dtype=object)

    def count_neighbors(self, query: np.ndarray, eps: np.ndarray) -> np.ndarray:
        return np.sum(self._metric(query[:, np.newaxis], self._data[np.newaxis]) < eps)
