from typing import Callable, TypeAlias

import numpy as np

Transformation: TypeAlias = Callable[[np.ndarray], np.ndarray]


def z_transform(x: np.ndarray) -> np.ndarray:
    """
    Z-transform the given data.
    """

    return (x - x.mean(axis=0)) / x.std(axis=0)


def rank_transform(x: np.ndarray, return_idxs: bool = False) -> np.ndarray:
    """
    Rank transform the given data.
    
    """

    idxs = np.argsort(x, axis=0)
    ranks = np.empty_like(x)
    np.put_along_axis(ranks, idxs, np.arange(x.shape[0])[:, np.newaxis], axis=0)
    
    if return_idxs:
        return ranks, idxs
    
    return ranks


_transformations = {'z': z_transform, 'rank': rank_transform}


def get_transformation(name: str) -> Transformation:
    """
    Get a transformation by name.
    """

    try:
        return _transformations[name]
    except KeyError:
        raise ValueError(f'Unknown transformation: {name}')
