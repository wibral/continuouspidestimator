from typing import Optional
from abc import ABC, abstractmethod

import numpy as np
from mpmath import mp

from csxpid.neighbors import BruteForceKNNFinder, SciPyKDTreesKNNFinder, KNNFinder
from csxpid.random_variable import ContinuousSpace, RandomVariable
from csxpid.lattice import Antichain, load_achains, load_moebius_inversion_matrix
from csxpid.transform import Transformation

digamma = np.vectorize(mp.digamma)


class CSxPIDEstimator(ABC):
    """
    Class for estimating continuous SxPID using a Kraskov-style estimator.
    """

    def __init__(self,
                 S_rvs: list[RandomVariable],
                 T_rv: RandomVariable,
                 knn_finder_class: type[KNNFinder],
                 k: int = 4,
                 transformation: Optional[Transformation] = None) -> None:
        """
        Initialise the estimator with the given k.

        Args:
            S_rvs: List of RandomVariables for the source samples.
            T_rv: RandomVariable for the target samples.
            knn_finder: A KNN_Finder object that finds the k nearest neighbors of a query vector in the given data.
            k: The number of nearest neighbors to use.
            transform: A Transformation object that transforms the data.
        """

        self._k = k
        self._S_rvs = S_rvs
        self._T_rv = T_rv
        self._knn_finder_class = knn_finder_class
        self._transformation = transformation

    def estimate(self,
                 S: np.ndarray,
                 T: np.ndarray,
                 antichains: Optional[list[Antichain]] = None,
                 transform_variables: bool = True) -> dict:
        """
        Estimate the continuous SxPID atoms of the given data.

        Args:
            S: Source samples. Numpy array of shape (n_samples, n_cumulative_source_dimensions).
            T: Target samples. A numpy array of shape (n_samples,).
            antichain: Optional list of antichains of the form [(set_1, ..., set_k)]. If None, all antichains are considered.
            transform_variables: Whether to transform the variables.

        Returns:
            A dictionary with antichain and PID values.
        """

        # Transform the variables if necessary
        if transform_variables and self._transformation is not None:
            S = self._transformation(S)
            T = self._transformation(T)

        # Get the antichains.
        if antichains is None:
            antichains = load_achains(S.shape[1])

        # Compute redundancies.
        redundancies = self.estimate_redundancies(
            S, T, antichains, transform_variables=False)

        # Compute the Moebius Inversion.
        M = load_moebius_inversion_matrix(S.shape[1])
        atoms = M @ redundancies

        # Format to atom dictionary.
        atom_dict = dict(zip(antichains, atoms))

        return atom_dict

    def estimate_redundancies(self,
                              S: np.ndarray,
                              T: np.ndarray,
                              antichains: list[Antichain],
                              transform_variables: bool = True) -> float | np.ndarray:
        """
        Estimate the continuous SxPID redundancies of the given data.

        Args:
            S: Source samples. Numpy array of shape (n_samples, n_cumulative_source_dimensions).
            T: Target samples. A numpy array of shape (n_samples,).
            antichains: List of antichains of the form [(set_1, ..., set_k)].
            transform_variables: Whether to transform the variables.

        Returns:
            float or numpy array of shape (n_antichains,).
        """
        return np.array([self.estimate_redundancy(S, T, antichain, transform_variables=transform_variables) for antichain in antichains])

    @abstractmethod
    def estimate_redundancy(self,
                            S: np.ndarray,
                            T: np.ndarray,
                            antichain: Antichain,
                            transform_variables: bool = True) -> float:
        """
        Estimate the continuous SxPID redundancy of the given data.

        Args:
            S: Source samples. Numpy array of shape (n_samples, n_cumulative_source_dimensions).
            T: Target samples. A numpy array of shape (n_samples,).
            antichain: Antichain of the form (set_1, ..., set_k).

        Returns:
            float.
        """


class PurelyContinuousSxPIDEstimator(CSxPIDEstimator):
    """
    Class for estimating purely continuous SxPID using a Kraskov-style estimator.
    """

    def __init__(self,
                 S_rvs: list[RandomVariable],
                 T_rv: RandomVariable,
                 knn_finder_class: type[KNNFinder],
                 knn_finder_kwargs: dict = {},
                 k: int = 4,
                 noise_stddev: float = 0,
                 transformation: Optional[Transformation] = None) -> None:
        """
        Initialise the estimator with the given k.

        Args:
            S_rvs: List of RandomVariables for the source samples.
            T_rv: RandomVariable for the target samples.
            knn_finder_class: A KNN_Finder class that finds the k nearest neighbors of a query vector in the given data.
            noise_stddev: Standard deviation of the Gaussian noise to add to the samples.
            transformation: A Transformation object that transforms the data.
        """

        super().__init__(S_rvs, T_rv, knn_finder_class, k, transformation)

        assert all([rv.is_continuous for rv in self._S_rvs]
                   ), "Source RandomVariables must be continuous."
        assert self._T_rv.is_continuous, "Target RandomVariable must be continuous."

        self._knn_finder_class = knn_finder_class
        self._knn_finder_kwargs = knn_finder_kwargs
        self._noise_stddev = noise_stddev

    def estimate_redundancy(self,
                            S: np.ndarray,
                            T: np.ndarray,
                            antichain: Antichain,
                            transform_variables: bool = True) -> np.ndarray:
        """
        Estimate the continuous SxPID redundancy I_cap for the given antichain.

        Args:
            S: Source samples. Numpy array of shape (n_samples, n_cumulative_source_dimensions).
            T: Target samples. A numpy array of shape (n_samples,).
            antichain: Tuple in the form of (set_1, ..., set_k).
            transform_variables: Whether to transform the variables.

        Returns:
            Redundancy I_cap in bits.
        """
        # Copy arrays to avoid side effects
        S = np.array(S)
        T = np.array(T)

        if transform_variables and self._transformation is not None:
            S = self._transformation(S)
            T = self._transformation(T)

        # If noise_stddev is not 0, add noise to the samples.
        if self._noise_stddev:
            S += np.random.normal(0, self._noise_stddev, S.shape)
            T += np.random.normal(0, self._noise_stddev, T.shape)

        # Computet epsilons
        eps_k = self._compute_epsilons(S, T, antichain)

        # Compute n_alpha and n_T
        n_alpha = self._compute_n_alpha(S, antichain, eps_k)
        mean_digamma_n_alpha = np.mean(digamma(n_alpha))
        del n_alpha

        n_T = self._compute_n_T(T, eps_k)
        mean_digamma_n_T = np.mean(digamma(n_T))
        del n_T

        # Compute redundancy.
        N = len(S)

        I_cap = digamma(self._k) + digamma(N) - \
            (mean_digamma_n_alpha + mean_digamma_n_T)

        return 1/np.log(2) * float(I_cap)

    def _compute_epsilons(self,
                          S: np.ndarray,
                          T: np.ndarray,
                          antichain: Antichain) -> np.ndarray:

        # Compute the index of the first dimension of each source variable.
        cum_dims = np.cumsum([0] + [s_var.measurable_space.dimension
                                    for s_var in self._S_rvs])

        S_split = np.split(S, cum_dims[1:-1], axis=1)

        # Shape: (n_samples, n_sets, n_neighbors)
        knn_dists = np.empty((len(S), len(antichain), self._k + 1))
        knn_indices = np.empty((len(S), len(antichain), self._k + 1), dtype=int)

        for a_idx, a in enumerate(antichain):

            ST_and = np.hstack([S_split[i] for i in a] + [T])

            ST_rvs_and = [self._S_rvs[j] for i in a for j in range(
                cum_dims[i], cum_dims[i+1])] + [self._T_rv]
            
            knn_finder_and = self._knn_finder_class(ST_rvs_and, ST_and, **self._knn_finder_kwargs)

            knn_dists[:, a_idx], knn_indices[:, a_idx] = knn_finder_and.get_neighbors(ST_and, k=self._k + 1)

        knn_dists = knn_dists.reshape(len(S), -1)
        knn_indices = knn_indices.reshape(len(S), -1)

        # Sort by distance.
        argsrt_indices = np.argsort(knn_dists, axis=1)
        knn_dists = np.take_along_axis(
            knn_dists, argsrt_indices, axis=1)
        knn_indices = np.take_along_axis(
            knn_indices, argsrt_indices, axis=1)

        # Remove duplicate indices.
        eps_k = np.zeros(len(S))
        for i in range(len(S)):
            _, unique_indices = np.unique(knn_indices[i],
                                          return_index=True)
            unique_dists = knn_dists[i][np.sort(unique_indices)]
            eps_k[i] = unique_dists[self._k]

        return eps_k

    def _compute_n_alpha(self,
                         S: np.ndarray,
                         antichain: Antichain,
                         eps_k: np.ndarray) -> np.ndarray:

        cum_dims = np.cumsum([0] + [s_var.measurable_space.dimension
                                    for s_var in self._S_rvs])

        S_split = np.split(S, cum_dims[1:-1], axis=1)

        # Shape: (len(S), len(antichain), number of neighbors in eps_k ball)
        knn_indices_and = np.empty((len(S), len(antichain)), dtype=object)
        
        for a_idx, a in enumerate(antichain):

            S_rvs_and = [self._S_rvs[j] for i in a for j in range(
                cum_dims[i], cum_dims[i+1])]

            # Get nearest neighbors in ball of radius eps_k in the individual "and"-spaces according to the antichain.
            S_and = np.hstack([S_split[i] for i in a])

            knn_finder_and = self._knn_finder_class(S_rvs_and, S_and, **self._knn_finder_kwargs)

            # Shape: (len(S), number of neighbors in eps_k ball)
            knn_indices_and[:, a_idx] = knn_finder_and.get_neighbors_ball(S_and, eps_k)

        # Compute number of unique neighbors from the neighbors in the individual S spaces.
        n_alpha = np.empty(len(S), dtype=int)
        for i in range(len(S)):
            combined_indices = np.concatenate([knn_indices_and[i][j]
                                               for j in range(len(antichain))], axis=0)
            n_alpha[i] = len(np.unique(combined_indices))

        return n_alpha

    def _compute_n_T(self,
                     T: np.ndarray,
                     eps_k: np.ndarray) -> np.ndarray:

        # Compute number of nearest neighbors in T space.
        knn_finder_T = self._knn_finder_class([self._T_rv],
                                              T,
                                              **self._knn_finder_kwargs)

        n_T = knn_finder_T.count_neighbors(T, eps_k)

        return n_T
