from dataclasses import dataclass
from typing import Optional

from csxpid.metric import Norm, Norms

@dataclass(frozen=True)
class ContinuousSpace():
    dimension: int = 1
    norm: Norm = Norms.EUCLIDIAN

@dataclass(frozen=True)
class DiscreteSpace():
    alphabet_size: Optional[int] = None
    
Space = ContinuousSpace | DiscreteSpace

@dataclass(frozen=True)
class RandomVariable():
    """A random variable.
    
    Has a measurable_space that the rv maps to.
    In the future, this space may be allowed to be the union of multiple continuous or categorical spaces.
    """
    measurable_space: Space
    
    def is_continuous(self) -> bool:
        return isinstance(self.measurable_space, ContinuousSpace)