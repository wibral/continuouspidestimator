# continuouspidestimator
#### A python package for the estimation of continuous shared-exclusion based partial information decomposition
[![License](https://img.shields.io/badge/License-BSD_3--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![Python 3.10](https://img.shields.io/badge/python-3.10-blue.svg)]()


The package provides a $k$-nearest-neighbor based estimator for a continuous version of the $I^\mathrm{sx}_\cap$ redundancy measure. The package further comprises functions to compute the associated PID atoms via a Moebius transformation for up to five source variables.

It has been developed as part of the publication [*Partial Information Decomposition for Continuous Variables based on Shared Exclusions*](https://arxiv.org/abs/2311.06373) which has been published on ArXiv in November 2023:

```
@misc{ehrlich2023partial,
      title={Partial Information Decomposition for Continuous Variables based on Shared Exclusions: Analytical Formulation and Estimation}, 
      author={David A. Ehrlich and Kyle Schick-Poland and Abdullah Makkeh and Felix Lanfermann and Patricia Wollstadt and Michael Wibral},
      year={2023},
      eprint={2311.06373},
      archivePrefix={arXiv},
      primaryClass={cs.IT}
}
```

### Contributions

The *continuouspidestimator* software package was jointly envisioned and designed by and [David A. Ehrlich](https://github.com/daehrlich) and [Kyle Schick-Poland](). The package was implemented by David A. Ehrlich and is actively maintained by  David A. Ehrlich and Kyle Schick-Poland.

### Installation
All requirements for the *continuouspidestimator* package are summarized in the *env.yaml* file.

We recommend setting up a new virtual environment using the conda command:

    conda env create -f env.yaml
    conda activate csxpid

Afterwards, the package can be installed using pip:

    pip install -e .