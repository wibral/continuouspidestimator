import numpy as np
from scipy.stats import multivariate_normal
import pytest

from csxpid.estimator import PurelyContinuousSxPIDEstimator
from csxpid.neighbors import SciPyKDTreesKNNFinder
from csxpid.random_variable import ContinuousSpace, RandomVariable

cs = [0.1, 0.5, 0.9, 0.99]


@pytest.mark.parametrize("c", cs)
def test_joint_gaussian(c: float) -> None:
    """Test CSxPID estimator with a joint Gaussian distribution.

    Compares the Redundancy I_cap obtained from a Monte-Carlo simulation to the result from the CSxPID estimator.
    """
    N_MC: int = 10_000_000
    N: int = 10_000
    np.random.seed(12345)

    # Create joint Gaussian distribution.
    mu_SST = np.array([0, 0, 0])
    cov_SST = np.array([[1, 0, c], [0, 1, 0], [c, 0, 1]])

    p_SST = multivariate_normal(mu_SST, cov_SST)

    # Create marginal Gaussian distributions.
    mu_S = np.array([0])
    cov_S = np.array([[1]])

    p_S = multivariate_normal(mu_S, cov_S)

    mu_T = np.array([0])
    cov_T = np.array([[1]])

    p_T = multivariate_normal(mu_T, cov_T)

    mu_S1T = np.array([0, 0])
    cov_S1T = np.array([[1, c], [c, 1]])

    mu_S2T = np.array([0, 0])
    cov_S2T = np.array([[1, 0], [0, 1]])

    p_S1T = multivariate_normal(mu_S1T, cov_S1T)
    p_S2T = multivariate_normal(mu_S2T, cov_S2T)

    # Compute CSxPID as Monte Carlo integration.
    samples = p_SST.rvs(size=N_MC)

    I_cap_mc = np.mean(np.log2(
        (p_S1T.pdf(samples[:, [0, 2]]) + p_S2T.pdf(samples[:, [1, 2]])) /
        ((p_S.pdf(samples[:, 0]) + p_S.pdf(samples[:, 1]))
         * p_T.pdf(samples[:, 2]))
    ))

    print(f'{I_cap_mc=} bits.')

    # Estimate CSxPID Redundancy from samples alone.
    rv = RandomVariable(measurable_space=ContinuousSpace(dimension=1))
    S = samples[:N, 0:2]
    T = samples[:N, 2:3]

    estimator = PurelyContinuousSxPIDEstimator(
        S_rvs=[rv] * 2, T_rv=rv, knn_finder_class=SciPyKDTreesKNNFinder)
    I_cap_est = estimator.estimate_redundancy(S=S, T=T, antichain=((0,), (1,)))

    print(f'{I_cap_est=} bits.')

    assert np.isclose(I_cap_est, I_cap_mc, atol=0.1)


if __name__ == '__main__':
    for c in cs:
        print(f'{c=}')
        test_joint_gaussian(c)
