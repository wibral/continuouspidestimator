import numpy as np
from sklearn.feature_selection import mutual_info_regression
import pytest

from csxpid.estimator import PurelyContinuousSxPIDEstimator
from csxpid.neighbors import SciPyKDTreesKNNFinder
from csxpid.random_variable import ContinuousSpace, RandomVariable
from csxpid.transform import Transformation, z_transform, rank_transform

noise_sigmas = [0.01, 0.1, 1.0]


@pytest.mark.parametrize(argnames=["noise_sigma", 'transformation'],
                         argvalues=[(noise_sigma, transformation)
                                    for noise_sigma in noise_sigmas
                                    for transformation in [None, z_transform, rank_transform]])
def test_AWGN_MI(noise_sigma: float, transformation: Transformation) -> None:
    """ Test if the CSxPID estimator returns the correct mutual information for a simple additive-white Gaussian noise channel."""

    rng = np.random.default_rng(12345)
    size = (5000, 1)

    X = rng.normal(size=size)
    Y = X + rng.normal(scale=noise_sigma, size=size)

    rv = RandomVariable(measurable_space=ContinuousSpace(dimension=1))

    # Compute Mutual Information analytiically.
    I_analytic = 1/2 * np.log2(1 + 1/noise_sigma**2)
    print(f"{I_analytic=} bits.")

    # Compare to sklearn mutual_info_regression.
    trafo_lambda = lambda x: x if transformation is None else transformation(x)
    I_sklearn = 1/np.log(2) * mutual_info_regression(
        trafo_lambda(X), trafo_lambda(Y)[:, 0], discrete_features=False, n_neighbors=4, random_state=12345)[0]
    print(f"{I_sklearn=} bits.")

    # Compare to CSxPID estimator.
    estimator = PurelyContinuousSxPIDEstimator(
        S_rvs=[rv], T_rv=rv, knn_finder_class=SciPyKDTreesKNNFinder, k=4, noise_stddev=1E-8, transformation=transformation)
    I_est = estimator.estimate_redundancy(S=X, T=Y, antichain=((0,),))
    print(f"{I_est=} bits.")

    assert np.allclose(I_est, I_analytic, rtol=0.1)
    assert np.allclose(I_est, I_sklearn, rtol=0.01)


if __name__ == "__main__":
    for noise_sigma in noise_sigmas:
        for transformation in [None, z_transform, rank_transform]:
            print(f"{noise_sigma=}, {transformation=}")
            test_AWGN_MI(noise_sigma, transformation)
    print("All tests passed.")
