import numpy as np

import pytest
from csxpid.metric import Norms

from csxpid.random_variable import ContinuousSpace, RandomVariable
from csxpid.neighbors import BruteForceKNNFinder, KNNFinder, SciPyKDTreesKNNFinder

finder_classes = [SciPyKDTreesKNNFinder, BruteForceKNNFinder]


@pytest.mark.parametrize("finder_class", finder_classes)
def test_get_neighbors_small(finder_class: type[KNNFinder]):
    """
    Test the neighbors function with a small dataset.
    """

    # Create a small dataset.
    data = np.array(
        [[0.3, 4.5], [0.7, 2.1], [0.1, 0.2], [0.9, 0.8], [0.6, 0.4]])

    # Configure Random Variables
    rv = RandomVariable(measurable_space=ContinuousSpace(
        dimension=1, norm=Norms.EUCLIDIAN))

    # Create a Knn finder.
    finder = finder_class([rv] * 2, data)

    # Define query point
    query = np.array([[0, 0], [2, 2]])

    # Find the nearest neighbors of the query point.
    neighbor_dists, neighbor_idcs = finder.get_neighbors(query, k=3)

    # Check the neighbors are correct.
    assert np.array_equal(neighbor_dists, [[0.2, 0.6, 0.9], [1.2, 1.3, 1.6]])
    assert np.array_equal(neighbor_idcs, [[2, 4, 3], [3, 1, 4]])


def test_get_neighbors_consistency():
    """
    Test that the finders returns the same results as the brute force finder.
    Computes k nearest neighbors of each query point in the data.
    """

    k = 3

    # Create large random dataset.
    n_vars = 5
    n_samples = 1000

    data = np.random.rand(n_samples, n_vars)

    # Configure Random Variables
    rv = RandomVariable(measurable_space=ContinuousSpace(
        dimension=1, norm=Norms.EUCLIDIAN))

    # Create KNN finders.
    finders = [finder_class([rv] * n_vars, data)
               for finder_class in finder_classes]

    # Find all nearest neighbors (query = data).
    neighbor_dists, neighbor_idcs = zip(
        *[finder.get_neighbors(data, k) for finder in finders])

    # Check consistency.
    assert all(np.array_equal(neighbor_dists[0], n)
               for n in neighbor_dists[1:])
    assert all(np.array_equal(neighbor_idcs[0], n) for n in neighbor_idcs[1:])


@pytest.mark.parametrize("finder_class", finder_classes)
def test_get_neighbors_ball_small(finder_class: type[KNNFinder]):

    # Create a small dataset.
    data = np.array([[0.3, 4.5], [0.7, 2.1], [0.1, 0.2], [1, 0.8], [0.6, 0.4]])

    # Configure Random Variables
    rv = RandomVariable(measurable_space=ContinuousSpace(
        dimension=1, norm=Norms.EUCLIDIAN))

    # Create a Knn finder.
    finder = finder_class([rv] * 2, data)

    # Define query point
    query = np.array([[0, 0], [2, 2]])
    eps = np.array([2, 2])

    # Find the nearest neighbors of the query point.
    neighbors = finder.get_neighbors_ball(query, eps=eps)

    # Check the neighbors are correct.
    assert (np.array_equal(a, b)
            for a, b in zip(neighbors, [[2, 3, 4], [1, 2, 3, 4]]))


if __name__ == '__main__':
    test_get_neighbors_small(BruteForceKNNFinder)
    test_get_neighbors_small(SciPyKDTreesKNNFinder)
    test_get_neighbors_consistency()
    test_get_neighbors_ball_small(BruteForceKNNFinder)
    test_get_neighbors_ball_small(SciPyKDTreesKNNFinder)
    print('All tests passed')
