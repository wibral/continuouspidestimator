import argparse
import os
import time

import numpy as np

from csxpid.random_variable import RandomVariable, ContinuousSpace
from csxpid.metric import Norms
from csxpid.transform import get_transformation
from csxpid.estimator import PurelyContinuousSxPIDEstimator
from csxpid.neighbors import SciPyKDTreesKNNFinder

def main():
    arggparser = argparse.ArgumentParser()
    arggparser.add_argument('--N', type=int)
    arggparser.add_argument('--eps', type=float, default=0.01)
    arggparser.add_argument('--gate', type=str)

    args = arggparser.parse_args()
    N = args.N
    gate = args.gate
    eps = args.eps

    rng = np.random.default_rng()

    print(f'Computing PID for {gate} gate with {N=} and {eps=}...')

    if gate == 'RED':

        S_1 = rng.normal(size=(N, 1))
        S_2 = S_1
        T = S_1 + rng.normal(scale=eps, size=(N, 1))

        rv = RandomVariable(measurable_space=ContinuousSpace())
        rv_S1 = rv
        rv_S2 = rv
        rv_T = rv
    
    elif gate == 'COPY':

        S_1 = rng.normal(size=(N, 1))
        S_2 = rng.normal(size=(N, 1))
        T = np.concatenate([S_1, S_2], axis=1) + rng.normal(scale=eps, size=(N, 2))
        print(T.shape)

        rv = RandomVariable(measurable_space=ContinuousSpace())
        rv_S1 = rv
        rv_S2 = rv
        rv_T = RandomVariable(measurable_space=ContinuousSpace(dimension=2, norm=Norms.MAX))

    elif gate == 'UNQ':

        S_1 = rng.normal(size=(N, 1))
        S_2 = rng.normal(size=(N, 1))
        T = S_1 + rng.normal(scale=eps, size=(N, 1))

        rv = RandomVariable(measurable_space=ContinuousSpace())
        rv_S1 = rv
        rv_S2 = rv
        rv_T = rv

    elif gate == 'SUM':

        S_1 = rng.normal(size=(N, 1))
        S_2 = rng.normal(size=(N, 1))
        T = S_1 + S_2 + rng.normal(0, eps, (N, 1))

        rv = RandomVariable(measurable_space=ContinuousSpace())
        rv_S1 = rv
        rv_S2 = rv
        rv_T = rv

    transform = None

    start = time.perf_counter()
    estimator = PurelyContinuousSxPIDEstimator(S_rvs=[rv_S1, rv_S2], 
                                               T_rv=rv_T, 
                                               k=4, 
                                               noise_stddev=1e-10, 
                                               knn_finder_class=SciPyKDTreesKNNFinder, 
                                               knn_finder_kwargs={'workers': -1}, 
                                               transformation=transform)
    
    pid = estimator.estimate(S=np.concatenate([S_1, S_2], axis=1), T=T)
    end = time.perf_counter()

    runtime = end-start

    print(f"Elapsed time: {end-start} seconds")

    red = pid[((0,), (1,))]
    unq1 = pid[((0,),)]
    unq2 = pid[((1,),)]
    syn = pid[((0, 1),)]

    # Save results:
    results_file = f"gates_convergence_results_{gate}.csv"

    # Write header if file does not exist:
    if not os.path.isfile(results_file):
        with open(results_file, 'w') as f:
            f.write("gate,N,eps,red,unq1,unq2,syn,time\n")

    # Append results:
    with open(results_file, 'a') as f:
        f.write(f"{gate},{N},{eps},{red},{unq1},{unq2},{syn},{runtime}\n")

if __name__ == '__main__':
    main()