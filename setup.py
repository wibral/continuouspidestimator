import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="csxpid",
    version="1.0.0",
    author="David A. Ehrlich and Kyle Schick-Poland",
    author_email="davidalexander.ehrlich@uni-goettingen.de",
    description="A nearest-neighbor-based estimator for continuous SxPID",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=setuptools.find_packages(),
    install_requires=["numpy", "scipy", "scikit-learn", "matplotlib", "pandas", "tqdm"],
    classifiers=["Programming Language :: Python :: 3", "Operating System :: OS Independent"],
    python_requires=">=3.6",
)